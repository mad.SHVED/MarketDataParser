﻿using IniParser;
using IniParser.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows;
using System.Drawing;
using System.Threading.Tasks;
using System.Diagnostics;

namespace MarketDataParser
{
    class Program
    {
        private static IniData iniData;
        private static MainWindow mainWindow;
        private static readonly object locker = new object();

        public static void InitializeProgram()
        {
            var iniParser = new FileIniDataParser();
            if (!File.Exists("config.ini"))
            {
                iniData = new IniData();

                iniData.Sections.AddSection("General");
                iniData.Sections.GetSectionData("General").Keys.AddKey("ValidTrendCount", "0");
                iniData.Sections.GetSectionData("General").Keys.AddKey("TimeRatio", "1,0");
                iniData.Sections.GetSectionData("General").Keys.AddKey("ForecastsPath", AppDomain.CurrentDomain.BaseDirectory + "forecasts\\");
                iniData.Sections.GetSectionData("General").Keys.AddKey("ThreadCount", Environment.ProcessorCount.ToString());
                iniData.Sections.GetSectionData("General").Keys.AddKey("SOInterval", "10");
                iniParser.WriteFile("config.ini", iniData);
            }
            iniData = iniParser.ReadFile("config.ini");

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            mainWindow = (MainWindow)Application.Current.MainWindow;

            Directory.CreateDirectory("database");
        }

        public static void CreateDataBaseIfNotExist()
        {
            if (!File.Exists(".\\database\\db.sqlite"))
            {
                WindowLog("Database not exist. Creating new");
                SQLiteConnection.CreateFile(".\\database\\db.sqlite");
            }
            else
                return;

            SQLiteCommand dbCmd = GetDbConnection(true);

            dbCmd.CommandText = "CREATE TABLE IF NOT EXISTS stocks (id INTEGER PRIMARY KEY, ticker TEXT, name TEXT, link TEXT, exchangeName TEXT, exchangeId INTEGER, lastUpdate STRING, sector STRING, industry STRING, sharesOutstanding STRING, inactive INTEGER, errors INTEGER)";
            dbCmd.ExecuteNonQuery();

            dbCmd.CommandText = "CREATE TABLE IF NOT EXISTS exchanges (id INTEGER PRIMARY KEY NOT NULL, country INTEGER NOT NULL, name TEXT NOT NULL, inactive INTEGER NOT NULL)";
            dbCmd.ExecuteNonQuery();

            dbCmd.CommandText = "CREATE TABLE IF NOT EXISTS timeFrames (id INTEGER PRIMARY KEY, value TEXT, name TEXT, step INTEGER, inactive INTEGER)";
            dbCmd.ExecuteNonQuery();

            dbCmd.CommandText = "CREATE TABLE IF NOT EXISTS dividentsCalendar (id INTEGER, timeStamp INTEGER)";
            dbCmd.ExecuteNonQuery();

            dbCmd.CommandText = "CREATE TABLE IF NOT EXISTS forecasts (stockId INTEGER PRIMARY KEY, ticker STRING, risingPulse INTEGER, upTrend INTEGER, downTrend INTEGER, validTrendCount INTEGER, authenticity REAL, currentPrice REAL, target REAL, potential REAL, model REAL)";
            dbCmd.ExecuteNonQuery();

            dbCmd.Parameters.AddWithValue("id", 1);
            dbCmd.Parameters.AddWithValue("country", 5);
            dbCmd.Parameters.AddWithValue("name", "NYSE");
            dbCmd.Parameters.AddWithValue("inactive", 0);
            dbCmd.CommandText = "INSERT OR REPLACE INTO exchanges (id, country, name, inactive) values (?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 2);
            dbCmd.Parameters.AddWithValue("country", 5);
            dbCmd.Parameters.AddWithValue("name", "NASDAQ");
            dbCmd.Parameters.AddWithValue("inactive", 0);
            dbCmd.CommandText = "INSERT OR REPLACE INTO exchanges (id, country, name, inactive) values (?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 40);
            dbCmd.Parameters.AddWithValue("country", 56);
            dbCmd.Parameters.AddWithValue("name", "MOEX");
            dbCmd.Parameters.AddWithValue("inactive", 0);
            dbCmd.CommandText = "INSERT OR REPLACE INTO exchanges (id, country, name, inactive) values (?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", -1);
            dbCmd.Parameters.AddWithValue("country", 0);
            dbCmd.Parameters.AddWithValue("name", "Currencies crosses");
            dbCmd.Parameters.AddWithValue("inactive", 0);
            dbCmd.CommandText = "INSERT OR REPLACE INTO exchanges (id, country, name, inactive) values (?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", -2);
            dbCmd.Parameters.AddWithValue("country", 0);
            dbCmd.Parameters.AddWithValue("name", "Commodity futures");
            dbCmd.Parameters.AddWithValue("inactive", 0);
            dbCmd.CommandText = "INSERT OR REPLACE INTO exchanges (id, country, name, inactive) values (?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 0);
            dbCmd.Parameters.AddWithValue("name", "One minute");
            dbCmd.Parameters.AddWithValue("value", "1");
            dbCmd.Parameters.AddWithValue("step", (long)259200);
            dbCmd.Parameters.AddWithValue("inactive", 1);
            dbCmd.CommandText = "INSERT OR REPLACE INTO timeFrames (id, name, value, step, inactive) values (?,?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 1);
            dbCmd.Parameters.AddWithValue("name", "Five minutes");
            dbCmd.Parameters.AddWithValue("value", "5");
            dbCmd.Parameters.AddWithValue("step", (long)1296000);
            dbCmd.Parameters.AddWithValue("inactive", 1);
            dbCmd.CommandText = "INSERT OR REPLACE INTO timeFrames (id, name, value, step, inactive) values (?,?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 2);
            dbCmd.Parameters.AddWithValue("name", "Fifteen minutes");
            dbCmd.Parameters.AddWithValue("value", "15");
            dbCmd.Parameters.AddWithValue("step", (long)3888000);
            dbCmd.Parameters.AddWithValue("inactive", 1);
            dbCmd.CommandText = "INSERT OR REPLACE INTO timeFrames (id, name, value, step, inactive) values (?,?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 3);
            dbCmd.Parameters.AddWithValue("name", "Thirty minutes");
            dbCmd.Parameters.AddWithValue("value", "30");
            dbCmd.Parameters.AddWithValue("step", (long)7776000);
            dbCmd.Parameters.AddWithValue("inactive", 1);
            dbCmd.CommandText = "INSERT OR REPLACE INTO timeFrames (id, name, value, step, inactive) values (?,?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 4);
            dbCmd.Parameters.AddWithValue("name", "One hour");
            dbCmd.Parameters.AddWithValue("value", "60");
            dbCmd.Parameters.AddWithValue("step", (long)15552000);
            dbCmd.Parameters.AddWithValue("inactive", 1);
            dbCmd.CommandText = "INSERT OR REPLACE INTO timeFrames (id, name, value, step, inactive) values (?,?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 5);
            dbCmd.Parameters.AddWithValue("name", "Five hours");
            dbCmd.Parameters.AddWithValue("value", "300");
            dbCmd.Parameters.AddWithValue("step", (long)77760000);
            dbCmd.Parameters.AddWithValue("inactive", 1);
            dbCmd.CommandText = "INSERT OR REPLACE INTO timeFrames (id, name, value, step, inactive) values (?,?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 6);
            dbCmd.Parameters.AddWithValue("name", "Day");
            dbCmd.Parameters.AddWithValue("value", "D");
            dbCmd.Parameters.AddWithValue("step", (long)373248000);
            dbCmd.Parameters.AddWithValue("inactive", 0);
            dbCmd.CommandText = "INSERT OR REPLACE INTO timeFrames (id, name, value, step, inactive) values (?,?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 7);
            dbCmd.Parameters.AddWithValue("name", "Week");
            dbCmd.Parameters.AddWithValue("value", "W");
            dbCmd.Parameters.AddWithValue("step", (long)2612736000);
            dbCmd.Parameters.AddWithValue("inactive", 1);
            dbCmd.CommandText = "INSERT OR REPLACE INTO timeFrames (id, name, value, step, inactive) values (?,?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 8);
            dbCmd.Parameters.AddWithValue("name", "Forty five minutes");
            dbCmd.Parameters.AddWithValue("value", "45");
            dbCmd.Parameters.AddWithValue("step", (long)11664000);
            dbCmd.Parameters.AddWithValue("inactive", 1);
            dbCmd.CommandText = "INSERT OR REPLACE INTO timeFrames (id, name, value, step, inactive) values (?,?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 9);
            dbCmd.Parameters.AddWithValue("name", "Two hours");
            dbCmd.Parameters.AddWithValue("value", "120");
            dbCmd.Parameters.AddWithValue("step", (long)31104000);
            dbCmd.Parameters.AddWithValue("inactive", 1);
            dbCmd.CommandText = "INSERT OR REPLACE INTO timeFrames (id, name, value, step, inactive) values (?,?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            dbCmd.Parameters.AddWithValue("id", 10);
            dbCmd.Parameters.AddWithValue("name", "Four hours");
            dbCmd.Parameters.AddWithValue("value", "240");
            dbCmd.Parameters.AddWithValue("step", (long)62208000);
            dbCmd.Parameters.AddWithValue("inactive", 1);
            dbCmd.CommandText = "INSERT OR REPLACE INTO timeFrames (id, name, value, step, inactive) values (?,?,?,?,?)";
            dbCmd.ExecuteNonQuery();
            dbCmd.Parameters.Clear();

            DisposeDbConnection(dbCmd);
        }

        public static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs exceptionEventArgs)
        {
            FileLog(GetExceptionMessage((Exception)exceptionEventArgs.ExceptionObject));
        }

        public static string GetExceptionMessage(Exception ex)
        {
            StackFrame[] stackTraceFrames = new StackTrace(ex).GetFrames();
            string message = ex.Message + ";\n";

            foreach (StackFrame stackFrame in stackTraceFrames)
            {
                message += stackFrame.GetMethod().Name + " at line " + stackFrame.GetNativeOffset() + ";\n";
            }

            return message;
        }

        public static void WindowLog(string message, bool replaceLast = false)
        {
            if (!replaceLast)
                mainWindow.Dispatcher.BeginInvoke(new Action(() => { mainWindow.logListView.Items.Add(DateTime.Now + ": " + message + ";"); }));
            else
            {
                mainWindow.Dispatcher.BeginInvoke(new Action(() =>
                {
                    mainWindow.logListView.Items.RemoveAt(mainWindow.logListView.Items.Count - 1);
                    mainWindow.logListView.Items.Add(DateTime.Now + ": " + message + ";");
                }));
            }
        }

        public static void FileLog(string message)
        {
            lock (locker)
            {
                StreamWriter streamWriter = File.AppendText("log.txt");
                streamWriter.WriteLine(DateTime.Now + ": " + message + "\n");
                streamWriter.Close();
            }
        }

        private static void UpdateFields()
        {
            SQLiteCommand dbCmd = GetDbConnection(true);

            dbCmd.CommandText = "UPDATE stocks SET errors = 0";
            dbCmd.ExecuteNonQuery();

            dbCmd.CommandText = "UPDATE stocks SET lastUpdate = 'n/a' WHERE lastUpdate IS NULL";
            dbCmd.ExecuteNonQuery();

            dbCmd.CommandText = "UPDATE stocks SET sharesOutstanding = 'n/a' WHERE sharesOutstanding IS NULL";
            dbCmd.ExecuteNonQuery();

            dbCmd.CommandText = "UPDATE stocks SET sector = 'n/a' WHERE sector IS NULL";
            dbCmd.ExecuteNonQuery();

            dbCmd.CommandText = "UPDATE stocks SET industry = 'n/a' WHERE industry IS NULL";
            dbCmd.ExecuteNonQuery();
            
            dbCmd.CommandText = "UPDATE stocks SET inactive = (SELECT inactive FROM exchanges WHERE id = stocks.exchangeId), exchangeName = (SELECT name FROM exchanges WHERE id = stocks.exchangeId)";
            dbCmd.ExecuteNonQuery();

            DisposeDbConnection(dbCmd);
        }

        public static DataTable GetDataTable(string query)
        {
            DataTable dataTable = new DataTable();
            SQLiteCommand dbCmd = GetDbConnection();
            dbCmd.CommandText = query;
            dataTable.Load(dbCmd.ExecuteReader());
            DisposeDbConnection(dbCmd);

            return dataTable;
        }

        private static void CleanupBase()
        {
            SQLiteCommand dbCmd;
            DataTable stocksTable = GetDataTable("SELECT * FROM stocks WHERE exchangeId NOT IN (SELECT id FROM exchanges)");
            DataTable timeFramesTable = GetDataTable("SELECT * FROM timeFrames");

            if (stocksTable.Rows.Count != 0)
            {
                dbCmd = GetDbConnection(true);
                foreach (DataRow stock in stocksTable.Rows)
                {
                    foreach (DataRow timeFrame in timeFramesTable.Rows)
                    {
                        string tableName = "marketData" + stock["id"].ToString() + "timeFrame" + timeFrame["value"].ToString();
                        dbCmd.CommandText = "DROP TABLE IF EXISTS " + tableName;
                        dbCmd.ExecuteNonQuery();
                    }

                    dbCmd.Parameters.AddWithValue("id", Convert.ToInt32(stock["id"]));
                    dbCmd.CommandText = "DELETE FROM dividentsCalendar WHERE id = :id";
                    dbCmd.ExecuteNonQuery();
                }

                dbCmd.CommandText = "DELETE FROM stocks WHERE exchangeId NOT IN (SELECT id FROM exchanges)";
                dbCmd.ExecuteNonQuery();
                DisposeDbConnection(dbCmd);
            }
        }

        private static SQLiteCommand GetDbConnection(bool beginTrasaction = false)
        {
            FileInfo dbFile = new FileInfo(".\\database\\db.sqlite");
            dbFile.IsReadOnly = false;

            var directory = new DirectoryInfo(".\\database");
            directory.Attributes &= ~FileAttributes.ReadOnly;

            directory = new DirectoryInfo(".\\");
            directory.Attributes &= ~FileAttributes.ReadOnly;

            SQLiteConnectionStringBuilder connectionStringBuilder = new SQLiteConnectionStringBuilder();
            connectionStringBuilder.DataSource = ".\\database\\db.sqlite";
            connectionStringBuilder.Version = 3;
            connectionStringBuilder.BusyTimeout = 120000;
            connectionStringBuilder.Pooling = false;
            connectionStringBuilder.JournalMode = SQLiteJournalModeEnum.Wal;
            connectionStringBuilder.SyncMode = SynchronizationModes.Normal;

            SQLiteConnection dbConnection = new SQLiteConnection(connectionStringBuilder.ToString());
            SQLiteCommand dbCmd = new SQLiteCommand();

            dbConnection.Open();
            dbCmd.Connection = dbConnection;

            if (beginTrasaction)
                dbCmd.Transaction = dbConnection.BeginTransaction();

            return dbCmd;
        }

        private static void DisposeDbConnection(SQLiteCommand dbCmd)
        {
            FileInfo dbFile = new FileInfo(".\\database\\db.sqlite");
            dbFile.IsReadOnly = false;

            var directory = new DirectoryInfo(".\\database");
            directory.Attributes &= ~FileAttributes.ReadOnly;

            directory = new DirectoryInfo(".\\");
            directory.Attributes &= ~FileAttributes.ReadOnly;

            if (dbCmd.Transaction != null)
                dbCmd.Transaction.Commit();

            dbCmd.Connection.Dispose();
            dbCmd.Dispose();
        }

        private static long GetLastTimeStamp(string tableName, string where = "")
        {
            SQLiteCommand dbCmd = GetDbConnection();

            dbCmd.CommandText = "SELECT MAX(timeStamp) FROM " + tableName + " " + where;
            var timeStamp = dbCmd.ExecuteScalar();
            dbCmd.Parameters.Clear();

            DisposeDbConnection(dbCmd);

            if (Convert.IsDBNull(timeStamp))
                return (long)0;

            return (long)timeStamp;
        }

        private static long GetTimeStampFromDateTime(DateTime dateTime)
        {
            long timeStamp = (long)(dateTime - new DateTime(1970, 1, 1)).TotalSeconds;
            return timeStamp;
        }

        //Update functions
        public static void UpdateData(bool stocks, bool marketData, bool dividentsCalendar, bool sharingOutstanding, bool forecast)
        {
            SQLiteCommand dbCmd;

            CleanupBase();
            UpdateFields();

            if (!stocks && !marketData && !forecast && !dividentsCalendar && !sharingOutstanding)
            {
                WindowLog("Nothing to do");
                return;
            }

            WindowLog("Starting");

            DataTable exchangesTable = GetDataTable("SELECT * FROM exchanges WHERE inactive = 0");
            if (stocks)
            {
                if (exchangesTable.Rows.Count == 0)
                {
                    WindowLog("Exchanges table is empty");
                    return;
                }

                foreach (DataRow exchange in exchangesTable.Rows)
                {
                    UpdateFields();
                    mainWindow.Dispatcher.Invoke(new Action(() => mainWindow.RefreshStocksDataGridAsync()));

                    dbCmd = GetDbConnection(true);
                    dbCmd.Parameters.AddWithValue("exchangeId", exchange["id"]);
                    dbCmd.CommandText = "UPDATE stocks SET inactive = 1 WHERE exchangeId = :exchangeId";
                    dbCmd.ExecuteNonQuery();
                    DisposeDbConnection(dbCmd);

                    if (Convert.ToInt32(exchange["id"]) == -1)
                    {
                        UpdateCurrenciesCrosses(exchange);
                        continue;
                    }

                    if (Convert.ToInt32(exchange["id"]) == -2)
                    {
                        UpdateCommodityFutures(exchange);
                        continue;
                    }

                    UpdateStocks(exchange);
                }
            }

            UpdateFields();
            mainWindow.Dispatcher.BeginInvoke(new Action(() => mainWindow.RefreshStocksDataGridAsync()));

            DataTable timeFramesTable = GetDataTable("SELECT * FROM timeFrames WHERE inactive = 0");
            DataTable stocksTable = GetDataTable("SELECT * FROM stocks WHERE inactive = 0");
            if (marketData)
            {
                if (timeFramesTable.Rows.Count == 0)
                {
                    WindowLog("All timeframes is inactive");
                    return;

                }
                if (stocksTable.Rows.Count == 0)
                {
                    WindowLog("Stocks table is empty");
                    return;

                }

                UpdateMarketData(stocksTable, timeFramesTable);

                while (true)
                {
                    DataTable errorStocksTable = GetDataTable("SELECT * FROM stocks WHERE errors = 1 AND inactive = 0");
                    UpdateFields();

                    if (errorStocksTable.Rows.Count != 0)
                    {
                        WindowLog("Market data updated with " + stocksTable.Rows.Count + " error(s). Trying to redownload data");
                        UpdateMarketData(errorStocksTable, timeFramesTable);
                    }
                    else
                        break;
                }
            }

            UpdateFields();
            mainWindow.Dispatcher.BeginInvoke(new Action(() => mainWindow.RefreshStocksDataGridAsync()));

            if (dividentsCalendar)
            {
                if (stocksTable.Rows.Count == 0)
                {
                    WindowLog("Stocks table is empty");
                    return;

                }

                UpdateDividentsCalendar(stocksTable);
            }

            UpdateFields();
            mainWindow.Dispatcher.BeginInvoke(new Action(() => mainWindow.RefreshStocksDataGridAsync()));

            if (sharingOutstanding)
            {
                if (stocksTable.Rows.Count == 0)
                {
                    WindowLog("Stocks table is empty");
                    return;

                }

                UpdateSharesOutstanding(stocksTable);
            }

            UpdateFields();
            mainWindow.Dispatcher.BeginInvoke(new Action(() => mainWindow.RefreshStocksDataGridAsync()));

            if (forecast)
                CreateForecasts();

            WindowLog("Success");
        }

        public static void UpdateStocks(DataRow exchange)
        {
            SQLiteCommand dbCmd;
            HttpWebRequest request;
            string responseString;
            byte[] parameters;
            int createdElements;
            int totalCount;
            int page;

            WindowLog(exchange["name"] + " - 0/0");

            page = 1;
            createdElements = 0;
            totalCount = 1;

            while (createdElements < totalCount)
            {
                request = WebRequest.CreateHttp("https://ru.investing.com/stock-screener/Service/SearchStocks");
                request.AutomaticDecompression = DecompressionMethods.GZip;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Headers.Add("X-Requested-With", "XMLHttpRequest");
                request.Headers.Add("Accept-Encoding", "gzip");
                request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";

                parameters = Encoding.ASCII.GetBytes("exchange[]=" + exchange["id"] + "&country[]=" + exchange["Country"] + "&pn=" + page + "&order[col]=viewData.symbol&order[dir]=a");
                request.GetRequestStream().Write(parameters, 0, parameters.Length);

                try
                {
                    responseString = new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd();
                }
                catch (Exception ex)
                {
                    WindowLog(exchange["name"] + " - server return error. Check log.txt file", true);
                    FileLog(GetExceptionMessage(ex));
                    return;
                }

                JObject responseJson = JsonConvert.DeserializeObject<JObject>(responseString);
                totalCount = responseJson["totalCount"].ToObject<int>();

                if (totalCount == 0)
                {
                    WindowLog(exchange["name"] + " - did not return data", true);
                    return;
                }

                dbCmd = GetDbConnection(true);

                foreach (var instrument in responseJson["hits"])
                {
                    dbCmd.Parameters.AddWithValue("id", instrument["pair_ID"].ToObject<int>());
                    dbCmd.Parameters.AddWithValue("ticker", instrument["stock_symbol"].ToString());
                    dbCmd.Parameters.AddWithValue("name", instrument["viewData"]["name"].ToString());
                    dbCmd.Parameters.AddWithValue("link", "https://ru.investing.com" + instrument["viewData"]["link"].ToString());
                    dbCmd.Parameters.AddWithValue("exchangeName", exchange["name"]);
                    dbCmd.Parameters.AddWithValue("exchangeId", exchange["id"]);
                    dbCmd.Parameters.AddWithValue("inactive", 0);
                    dbCmd.Parameters.AddWithValue("sector", instrument["sector_trans"].ToString() == "" ? "n/a" : instrument["sector_trans"].ToString());
                    dbCmd.Parameters.AddWithValue("industry", instrument["industry_trans"].ToString() == "" ? "n/a" : instrument["industry_trans"].ToString());
                    dbCmd.Parameters.AddWithValue("lastUpdateId", instrument["pair_ID"].ToObject<int>());
                    dbCmd.Parameters.AddWithValue("sharesOutstandingId", instrument["pair_ID"].ToObject<int>());

                    dbCmd.CommandText = "INSERT OR REPLACE INTO stocks (id, ticker, name, link, exchangeName, exchangeId, inactive, sector, industry, lastUpdate, sharesOutstanding) values (?,?,?,?,?,?,?,?,?,(SELECT lastUpdate FROM stocks WHERE id = :lastUpdateId),(SELECT sharesOutstanding FROM stocks WHERE id = :sharesOutstandingId))";
                    dbCmd.ExecuteNonQuery();
                    dbCmd.Parameters.Clear();

                    createdElements++;
                    WindowLog(exchange["name"] + " - " + createdElements + "/" + totalCount, true);
                }
                page++;

                DisposeDbConnection(dbCmd);
            }
        }

        private static void UpdateDividentsCalendar(DataTable stocksTable)
        {
            int updatedStocks = 0;

            WindowLog("Updating dividents calendar - " + updatedStocks + "/" + stocksTable.Rows.Count);

            foreach (DataRow stock in stocksTable.Rows)
            {
                SQLiteCommand dbCmd;
                HttpWebRequest request;
                HtmlAgilityPack.HtmlDocument dividentsPage;
                int stockId = Convert.ToInt32(stock["id"]);
                long lastTimeStamp = GetTimeStampFromDateTime(DateTime.Now) + (long)31536000;
                bool hasMoreHistory = true;
                string responseString;
                byte[] parameters;

                while (hasMoreHistory)
                {
                    request = WebRequest.CreateHttp("https://ru.investing.com/equities/MoreDividendsHistory");
                    request.AutomaticDecompression = DecompressionMethods.GZip;
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.Headers.Add("X-Requested-With", "XMLHttpRequest");
                    request.Headers.Add("Accept-Encoding", "gzip");
                    request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";

                    parameters = Encoding.ASCII.GetBytes("pairID=" + stockId + "&last_timestamp=" + lastTimeStamp);
                    request.GetRequestStream().Write(parameters, 0, parameters.Length);

                    responseString = new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd();
                    JObject responseJson = JsonConvert.DeserializeObject<JObject>(responseString);

                    dividentsPage = new HtmlAgilityPack.HtmlDocument();
                    dividentsPage.LoadHtml(responseJson["historyRows"].ToString());

                    hasMoreHistory = responseJson["hasMoreHistory"].ToString() != "";

                    dbCmd = GetDbConnection(true);
                    foreach (var childNode in dividentsPage.DocumentNode.ChildNodes)
                    {
                        if (childNode.Name == "tr")
                        {
                            lastTimeStamp = Convert.ToInt64(childNode.Attributes["event_timestamp"].Value);

                            if (lastTimeStamp == GetLastTimeStamp("dividentsCalendar", "WHERE id = " + stockId))
                            {
                                hasMoreHistory = false;
                                break;
                            }

                            dbCmd.Parameters.AddWithValue("id", stockId); ;
                            dbCmd.Parameters.AddWithValue("timeStamp", lastTimeStamp);
                            dbCmd.CommandText = "INSERT INTO dividentsCalendar (id, timeStamp) values (?, ?)";
                            dbCmd.ExecuteNonQuery();
                            dbCmd.Parameters.Clear();
                        }
                    }
                    DisposeDbConnection(dbCmd);
                }
                updatedStocks++;
                WindowLog("Updating dividents calendar - " + updatedStocks + "/" + stocksTable.Rows.Count, true);
            }
        }

        private static void UpdateSharesOutstanding(DataTable stocksTable)
        {
            int interval = 0;
            int updatedStocks = 0;

            try
            {
                interval = (int)(Convert.ToDouble(iniData["General"]["SOInterval"]) * 1000);
            }
            catch (Exception ex)
            {
                FileLog(GetExceptionMessage(ex));
                WindowLog("Error getting interval");
                return;
            }
            
            WindowLog("Updating sharing outstanding - " + updatedStocks + "/" + stocksTable.Rows.Count);

            foreach (DataRow stock in stocksTable.Rows)
            {
                SQLiteCommand dbCmd;
                HttpWebRequest request;
                HtmlAgilityPack.HtmlDocument page;
                string responseString;

                Uri uri = new Uri(stock["link"].ToString().Replace("ru.", "www."));
                request = WebRequest.CreateHttp(uri);
                request.AutomaticDecompression = DecompressionMethods.GZip;
                request.Method = "GET";
                request.Headers.Add("Accept-Encoding", "gzip");
                request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";

                try
                {
                    responseString = new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd();
                }
                catch (WebException ex)
                {
                    FileLog(GetExceptionMessage(ex));
                    updatedStocks++;
                    WindowLog("Updating sharing outstanding - " + updatedStocks + "/" + stocksTable.Rows.Count, true);
                    continue;
                }

                page = new HtmlAgilityPack.HtmlDocument();
                page.LoadHtml(responseString);

                string sharesOutstanding = "n/a";
                var nodes = page.DocumentNode.SelectNodes("//div[contains(@class, 'inlineblock')]");

                if (nodes != null)
                {
                    foreach (var node in nodes)
                    {
                        for (int index = 0; index < node.ChildNodes.Count; index++)
                        {
                            if (node.ChildNodes[index].InnerText == "Shares Outstanding")
                            {
                                sharesOutstanding = node.ChildNodes[index + 1].InnerText;
                            }
                        }
                    }
                }

                if (sharesOutstanding == "")
                    sharesOutstanding = "n/a";

                dbCmd = GetDbConnection(true);
                dbCmd.Parameters.AddWithValue("id", Convert.ToInt32(stock["id"]));
                dbCmd.Parameters.AddWithValue("sharesOutstanding", sharesOutstanding.ToLower());
                dbCmd.CommandText = "UPDATE stocks SET sharesOutstanding = :sharesOutstanding WHERE id = :id";
                dbCmd.ExecuteNonQuery();
                DisposeDbConnection(dbCmd);

                updatedStocks++;
                WindowLog("Updating sharing outstanding - " + updatedStocks + "/" + stocksTable.Rows.Count, true);

                Thread.Sleep(interval);
            }
        }

        private static void UpdateCurrenciesCrosses(DataRow exchange)
        {
            SQLiteCommand dbCmd;
            HttpWebRequest request;
            HtmlAgilityPack.HtmlDocument currenciesCrossesPage;
            string responseString;
            int createdElements;
            int totalCount;

            WindowLog("Currencies crosses - 0/0");

            request = WebRequest.CreateHttp("https://ru.investing.com/currencies/streaming-forex-rates-majors");
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Method = "GET";
            request.Headers.Add("Accept-Encoding", "gzip");
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";

            try
            {
                responseString = new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd();
            }
            catch (WebException ex)
            {
                WindowLog("Currencies crosses - server return error. Check log.txt file", true);
                FileLog(GetExceptionMessage(ex));
                return;
            }

            currenciesCrossesPage = new HtmlAgilityPack.HtmlDocument();
            currenciesCrossesPage.LoadHtml(responseString);

            var currenciesCrossesTable = currenciesCrossesPage.GetElementbyId("cr1").ChildNodes["tbody"].ChildNodes;
            totalCount = currenciesCrossesTable.Count;
            createdElements = 0;

            dbCmd = GetDbConnection(true);

            foreach (var tableRow in currenciesCrossesTable)
            {
                dbCmd.Parameters.AddWithValue("id", Convert.ToInt32(tableRow.ChildNodes[1].LastChild.Attributes["data-id"].Value));
                dbCmd.Parameters.AddWithValue("ticker", tableRow.ChildNodes[1].LastChild.Attributes["data-name"].Value);
                dbCmd.Parameters.AddWithValue("name", tableRow.ChildNodes[1].FirstChild.Attributes["title"].Value);
                dbCmd.Parameters.AddWithValue("link", "https://ru.investing.com" + tableRow.ChildNodes[1].FirstChild.Attributes["href"].Value);
                dbCmd.Parameters.AddWithValue("exchangeName", exchange["name"]);
                dbCmd.Parameters.AddWithValue("exchangeId", exchange["id"]);
                dbCmd.Parameters.AddWithValue("inactive", 0);
                dbCmd.Parameters.AddWithValue("lastUpdateId", Convert.ToInt32(tableRow.ChildNodes[1].LastChild.Attributes["data-id"].Value));
                dbCmd.CommandText = "INSERT OR REPLACE INTO stocks (id, ticker, name, link, exchangeName, exchangeId, inactive, lastUpdate) values (?,?,?,?,?,?,?,(SELECT lastUpdate FROM stocks WHERE id = :lastUpdateId))";
                dbCmd.ExecuteNonQuery();
                dbCmd.Parameters.Clear();

                createdElements++;
                WindowLog("Currencies crosses - " + createdElements + "/" + totalCount, true);
            }

            DisposeDbConnection(dbCmd);
        }

        private static void UpdateCommodityFutures(DataRow exchange)
        {
            SQLiteCommand dbCmd;
            HttpWebRequest request;
            HtmlAgilityPack.HtmlDocument commodityFuturesPage;
            string responseString;
            int createdElements;
            int totalCount;

            WindowLog("Commodity futures - 0/0");

            request = WebRequest.CreateHttp("https://ru.investing.com/commodities/real-time-futures");
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Method = "GET";
            request.Headers.Add("Accept-Encoding", "gzip");
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";

            try
            {
                responseString = new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd();
            }
            catch (WebException ex)
            {
                WindowLog("Commodity futures - server return error. Check log.txt file", true);
                FileLog(GetExceptionMessage(ex));
                return;
            }

            commodityFuturesPage = new HtmlAgilityPack.HtmlDocument();
            commodityFuturesPage.LoadHtml(responseString);

            var commodityFuturesTable = commodityFuturesPage.GetElementbyId("cross_rate_1").ChildNodes["tbody"].ChildNodes;
            totalCount = (commodityFuturesTable.Count - 1) / 2;
            createdElements = 0;

            dbCmd = GetDbConnection(true);

            foreach (var tableRow in commodityFuturesTable)
            {
                if (tableRow.Name == "#text")
                    continue;

                dbCmd.Parameters.AddWithValue("id", Convert.ToInt32(tableRow.ChildNodes[3].LastChild.Attributes["data-id"].Value));
                dbCmd.Parameters.AddWithValue("ticker", tableRow.ChildNodes[3].LastChild.Attributes["data-name"].Value);
                dbCmd.Parameters.AddWithValue("name", tableRow.ChildNodes[3].FirstChild.Attributes["title"].Value);
                dbCmd.Parameters.AddWithValue("link", "https://ru.investing.com" + tableRow.ChildNodes[3].FirstChild.Attributes["href"].Value);
                dbCmd.Parameters.AddWithValue("exchangeName", exchange["name"]);
                dbCmd.Parameters.AddWithValue("exchangeId", exchange["id"]);
                dbCmd.Parameters.AddWithValue("inactive", 0);
                dbCmd.Parameters.AddWithValue("lastUpdateId", Convert.ToInt32(tableRow.ChildNodes[3].LastChild.Attributes["data-id"].Value));
                dbCmd.CommandText = "INSERT OR REPLACE INTO stocks (id, ticker, name, link, exchangeName, exchangeId, inactive, lastUpdate) values (?,?,?,?,?,?,?,(SELECT lastUpdate FROM stocks WHERE id = :lastUpdateId))";
                dbCmd.ExecuteNonQuery();
                dbCmd.Parameters.Clear();

                createdElements++;
                WindowLog("Commodity futures - " + createdElements + "/" + totalCount, true);
            }

            DisposeDbConnection(dbCmd);
        }

        private static void UpdateMarketData(DataTable stocksTable, DataTable timeFramesTable)
        {
            var workingThreads = new List<Thread>();
            int threadCount = Convert.ToInt32(iniData["General"]["ThreadCount"]);
            int updatedStocks = 0;

            WindowLog("Updating market data - " + updatedStocks + "/" + stocksTable.Rows.Count);

            foreach (DataRow stock in stocksTable.Rows)
            {
                while (workingThreads.Count == threadCount)
                {
                    for (int index = workingThreads.Count - 1; index >= 0; index--)
                    {
                        Thread workingThread = workingThreads[index];
                        if (!workingThread.IsAlive)
                        {
                            workingThreads.Remove(workingThread);
                            updatedStocks++;
                            WindowLog("Updating market data - " + updatedStocks + "/" + stocksTable.Rows.Count, true);
                        }
                    }

                    if (workingThreads.Count == threadCount)
                        Thread.Sleep(100);
                }

                Thread newThread = new Thread(() => GetStockMarketData(stock, timeFramesTable));
                newThread.IsBackground = true;
                newThread.Start();
                workingThreads.Add(newThread);
            }

            foreach (Thread workingThread in workingThreads)
            {
                if (workingThread.IsAlive)
                    workingThread.Join();

                updatedStocks++;
                WindowLog("Updating market data - " + updatedStocks + "/" + stocksTable.Rows.Count, true);
            }
        }

        public static void GetStockMarketData(DataRow stock, DataTable timeFramesTable)
        {
            SQLiteCommand dbCmd;
            int stockId = Convert.ToInt32(stock["id"]);

            foreach (DataRow timeFrame in timeFramesTable.Rows)
            {
                JObject responseJson = null;
                long timeFrameStep = (long)timeFrame["step"];
                string timeFrameValue = timeFrame["value"].ToString();
                string tableName = "marketData" + stockId + "timeFrame" + timeFrameValue;
                bool clearTable = false;
                bool tableExists = false;

                dbCmd = GetDbConnection(true);
                dbCmd.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName + " (timeStamp INTEGER PRIMARY KEY, c REAL, o REAL, h REAL, l REAL, v REAL, trend INTEGER)";
                if (dbCmd.ExecuteNonQuery() == -1)
                {
                    tableExists = true;
                }
                DisposeDbConnection(dbCmd);

                if (tableExists)
                {
                    clearTable = ComparePreviousData(stockId, tableName, timeFrameStep / 10, timeFrameValue);
                    if (clearTable)
                    {
                        dbCmd = GetDbConnection(true);
                        dbCmd.CommandText = "DELETE FROM " + tableName;
                        dbCmd.ExecuteNonQuery();
                        DisposeDbConnection(dbCmd);
                    }
                }

                long from = GetLastTimeStamp(tableName);
                long to = 0;
                long now = GetTimeStampFromDateTime(DateTime.Now);

                while (to < now)
                {
                    to = from + timeFrameStep;

                    var request = WebRequest.CreateHttp("https://tvc4.forexpros.com/079b34920c3c048f3adb1301e4ae07cc/" + now + "/7/7/18/history" + "?symbol=" + stockId + "&resolution=" + timeFrameValue + "&from=" + from + "&to=" + to);
                    request.Method = "GET";
                    request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";

                    try
                    {
                        string responseString = new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd();
                        responseJson = JsonConvert.DeserializeObject<JObject>(responseString);
                    }
                    catch (Exception ex)
                    {
                        FileLog(GetExceptionMessage(ex));

                        dbCmd = GetDbConnection(true);
                        dbCmd.Parameters.AddWithValue("id", stockId);
                        dbCmd.CommandText = "UPDATE stocks SET errors = 1 WHERE id = :id";
                        dbCmd.ExecuteNonQuery();
                        DisposeDbConnection(dbCmd);

                        Thread.CurrentThread.Abort();
                    }

                    from = to;
                    if ((string)responseJson["s"] != "ok")
                        continue;

                    int index = 0;
                    dbCmd = GetDbConnection(true);
                    foreach (long timeStamp in responseJson["t"])
                    {
                        int trend = 0;

                        double c = (double)responseJson["c"][index];
                        double o = (double)responseJson["o"][index];
                        double h = (double)responseJson["h"][index];
                        double l = (double)responseJson["l"][index];

                        bool currentDayCondition = c > o;
                        bool prevuousDayCondition = false;
                        bool differenceCondition = false;

                        if (index == 0)
                        {
                            DataTable previousData = new DataTable();
                            dbCmd.CommandText = "SELECT * FROM " + tableName + " WHERE timeStamp = " + GetLastTimeStamp(tableName);
                            previousData.Load(dbCmd.ExecuteReader());

                            if (previousData.Rows.Count != 0)
                            {
                                prevuousDayCondition = (double)previousData.Rows[0]["c"] > (double)previousData.Rows[0]["o"];
                                differenceCondition = (h - l) > ((double)previousData.Rows[0]["h"] - (double)previousData.Rows[0]["l"]);

                                if (currentDayCondition && prevuousDayCondition && differenceCondition)
                                    trend = 1;
                                if (!currentDayCondition && !prevuousDayCondition && differenceCondition)
                                    trend = -1;
                            }
                        }
                        else
                        {
                            prevuousDayCondition = (double)responseJson["c"][index - 1] > (double)responseJson["o"][index - 1];
                            differenceCondition = (h - l) > ((double)responseJson["c"][index - 1] - (double)responseJson["o"][index - 1]);

                            if (currentDayCondition && prevuousDayCondition && differenceCondition)
                                trend = 1;
                            if (!currentDayCondition && !prevuousDayCondition && differenceCondition)
                                trend = -1;
                        }

                        dbCmd.Parameters.AddWithValue("timeStamp", timeStamp);
                        dbCmd.Parameters.AddWithValue("c", c);
                        dbCmd.Parameters.AddWithValue("o", o);
                        dbCmd.Parameters.AddWithValue("h", h);
                        dbCmd.Parameters.AddWithValue("l", l);

                        if (responseJson["v"][index].ToString() != "n/a")
                            dbCmd.Parameters.AddWithValue("v", (long)responseJson["v"][index]);
                        else
                            dbCmd.Parameters.AddWithValue("v", DBNull.Value);

                        dbCmd.Parameters.AddWithValue("trend", trend);

                        dbCmd.CommandText = "INSERT OR REPLACE INTO " + tableName + " (timeStamp, c, o, h, l, v, trend) values (?,?,?,?,?,?,?)";
                        dbCmd.ExecuteNonQuery();
                        dbCmd.Parameters.Clear();
                        index++;
                    }
                    DisposeDbConnection(dbCmd);
                }
            }

            dbCmd = GetDbConnection(true);
            dbCmd.Parameters.AddWithValue("id", stockId);
            dbCmd.Parameters.AddWithValue("lastUpdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            dbCmd.CommandText = "UPDATE stocks SET lastUpdate = :lastUpdate WHERE id = :id";
            dbCmd.ExecuteNonQuery();
            DisposeDbConnection(dbCmd);
        }

        public static void CreateForecasts()
        {
            long monthTimeStamp = GetTimeStampFromDateTime(DateTime.Now) - 2592000;
            int errors = 0;
            int progress = 0;
            DataTable stocksTable = GetDataTable("SELECT * FROM stocks WHERE inactive = 0"); ;

            SQLiteCommand dbCmd = GetDbConnection(true);
            dbCmd.CommandText = "DELETE FROM forecasts";
            dbCmd.ExecuteNonQuery();

            WindowLog("Creating forecasts - " + progress + "/" + stocksTable.Rows.Count);

            foreach (DataRow stock in stocksTable.Rows)
            {
                progress++;

                int stockId = Convert.ToInt32(stock["id"]);
                string tableName = "marketData" + stockId + "timeFrame" + "D";
                long lastTimeStamp = GetLastTimeStamp(tableName);
                SQLiteDataReader dbCmdReader;

                dbCmd.Parameters.AddWithValue("monthTimeStamp", monthTimeStamp);

                dbCmd.CommandText = "SELECT timeStamp, max(h) AS value FROM " + tableName + " WHERE timeStamp >= :monthTimeStamp";
                dbCmdReader = dbCmd.ExecuteReader();
                double firstLineMaxValue = 0;
                long firstLineMaxTimeStamp = 0;

                if (dbCmdReader.Read())
                {
                    if (Convert.IsDBNull(dbCmdReader.GetValue(0)))
                    {
                        errors++;
                        dbCmdReader.Close();
                        continue;
                    }
                    firstLineMaxValue = dbCmdReader.GetDouble(1);
                    firstLineMaxTimeStamp = dbCmdReader.GetInt64(0);
                }
                else
                {
                    errors++;
                    dbCmdReader.Close();
                    continue;
                }

                dbCmdReader.Close();

                dbCmd.CommandText = "SELECT timeStamp, min(l) AS value FROM " + tableName + " WHERE timeStamp >= :monthTimeStamp";
                dbCmdReader = dbCmd.ExecuteReader();
                double firstLineMinValue = 0;
                long firstLineMinTimeStamp = 0;

                if (dbCmdReader.Read())
                {
                    if (Convert.IsDBNull(dbCmdReader.GetValue(0)))
                    {
                        errors++;
                        dbCmdReader.Close();
                        continue;
                    }
                    firstLineMinValue = dbCmdReader.GetDouble(1);
                    firstLineMinTimeStamp = dbCmdReader.GetInt64(0);
                }
                else
                {
                    errors++;
                    dbCmdReader.Close();
                    continue;
                }

                dbCmdReader.Close();
                dbCmd.Parameters.Clear();

                bool risingPulse = firstLineMaxTimeStamp > firstLineMinTimeStamp;
                long startTimeStamp = risingPulse ? firstLineMinTimeStamp : firstLineMaxTimeStamp;
                long endTimeStamp = risingPulse ? firstLineMaxTimeStamp : firstLineMinTimeStamp;

                int upTrend = 0;
                int downTrend = 0;
                dbCmd.Parameters.AddWithValue("startTimeStamp", startTimeStamp);
                dbCmd.Parameters.AddWithValue("endTimeStamp", endTimeStamp);
                dbCmd.CommandText = "SELECT trend FROM " + tableName + " WHERE timeStamp >= :startTimeStamp AND timeStamp <= :endTimeStamp";
                dbCmdReader = dbCmd.ExecuteReader();

                while (dbCmdReader.Read())
                {
                    if (dbCmdReader.GetInt32(0) == 1)
                        upTrend += 1;
                    if (dbCmdReader.GetInt32(0) == -1)
                        downTrend += 1;
                }

                if (upTrend == 0 & downTrend == 0)
                {
                    errors++;
                    dbCmdReader.Close();
                    continue;
                }

                dbCmdReader.Close();
                dbCmd.Parameters.Clear();

                bool validTrendCount = risingPulse ? Convert.ToInt32(iniData["General"]["ValidTrendCount"]) <= upTrend : Convert.ToInt32(iniData["General"]["ValidTrendCount"]) <= downTrend;
                double authenticity = (double)(risingPulse ? upTrend : downTrend) / (double)(upTrend + downTrend);
                authenticity = Math.Round(authenticity, 2);
                double target = 0;
                double model = 0;

                if (lastTimeStamp != endTimeStamp)
                {
                    double averageFirstLineValue = (firstLineMaxValue - firstLineMinValue) / 2 + firstLineMinValue;
                    double ratio = 0;

                    dbCmd.Parameters.AddWithValue("averageFirstLineValue", averageFirstLineValue);
                    dbCmd.Parameters.AddWithValue("endTimeStamp", endTimeStamp);
                    dbCmd.CommandText = "SELECT max(timeStamp) AS timeStamp FROM " + tableName + " WHERE l < :averageFirstLineValue AND h > :averageFirstLineValue AND timeStamp > :endTimeStamp";
                    dbCmdReader = dbCmd.ExecuteReader();

                    if (dbCmdReader.Read())
                    {
                        if (!Convert.IsDBNull(dbCmdReader.GetValue(0)))
                        {
                            long lastAverageValueTimeStamp = dbCmdReader.GetInt64(0);
                            double timeRatio = Convert.ToDouble(iniData["General"]["TimeRatio"]);

                            ratio = (double)(lastAverageValueTimeStamp - endTimeStamp) / (double)(endTimeStamp - startTimeStamp);
                            if (ratio > timeRatio)
                                model = 4.236;
                        }
                    }

                    dbCmdReader.Close();
                    dbCmd.Parameters.Clear();

                    if (model == 0)
                    {
                        dbCmd.Parameters.AddWithValue("endTimeStamp", endTimeStamp);
                        dbCmd.CommandText = risingPulse ? "SELECT timeStamp, min(l) as value FROM " + tableName + " WHERE timeStamp > :endTimeStamp" : "SELECT timeStamp, max(h) as value FROM " + tableName + " WHERE timeStamp > :endTimeStamp";
                        dbCmdReader = dbCmd.ExecuteReader();

                        if (dbCmdReader.Read())
                        {
                            double secondLineValue = dbCmdReader.GetDouble(1);
                            long secondLineTimeStamp = dbCmdReader.GetInt64(0);
                            ratio = risingPulse ? (firstLineMaxValue - secondLineValue) / (firstLineMaxValue - firstLineMinValue) : (secondLineValue - firstLineMinValue) / (firstLineMaxValue - firstLineMinValue);

                            if (ratio > 0.5)
                                model = 1.618;
                            else
                                model = 2.618;
                        }

                        dbCmdReader.Close();
                        dbCmd.Parameters.Clear();
                    }

                    if (model != 0)
                    {
                        target = risingPulse ? (firstLineMaxValue - firstLineMinValue) * model + firstLineMinValue : firstLineMaxValue - (firstLineMaxValue - firstLineMinValue) * model;

                        if (target < 0 & model == 4.236)
                        {
                            model = 2.618;
                            target = risingPulse ? (firstLineMaxValue - firstLineMinValue) * model + firstLineMinValue : firstLineMaxValue - (firstLineMaxValue - firstLineMinValue) * model;
                        }

                        if (target < 0 & model == 2.618)
                        {
                            model = 1.618;
                            target = risingPulse ? (firstLineMaxValue - firstLineMinValue) * model + firstLineMinValue : firstLineMaxValue - (firstLineMaxValue - firstLineMinValue) * model;
                        }

                        if (target < 0)
                        {
                            errors++;
                            continue;
                        }

                        target = Math.Round(target, 6);
                    }
                }

                dbCmd.CommandText = "SELECT c FROM " + tableName + " ORDER BY timeStamp DESC LIMIT 1";
                var currentPrice = (double)dbCmd.ExecuteScalar();
                double potential = 0;

                if (!Convert.IsDBNull(currentPrice))
                {
                    currentPrice = Math.Round(currentPrice, 6);
                    if (target != 0)
                        potential = Math.Round((target / currentPrice) * 100 - 100, 6);
                }

                dbCmd.Parameters.AddWithValue("stockId", stockId);
                dbCmd.Parameters.AddWithValue("ticker", stock["ticker"].ToString());
                dbCmd.Parameters.AddWithValue("risingPulse", risingPulse);
                dbCmd.Parameters.AddWithValue("upTrend", upTrend);
                dbCmd.Parameters.AddWithValue("downTrend", downTrend);
                dbCmd.Parameters.AddWithValue("validTrendCount", validTrendCount);
                dbCmd.Parameters.AddWithValue("authenticity", authenticity);
                dbCmd.Parameters.AddWithValue("currentPrice", currentPrice);

                if (target != 0)
                    dbCmd.Parameters.AddWithValue("target", target);
                else
                    dbCmd.Parameters.AddWithValue("target", DBNull.Value);

                if (potential != 0)
                    dbCmd.Parameters.AddWithValue("potential", potential);
                else
                    dbCmd.Parameters.AddWithValue("potential", DBNull.Value);

                if (model != 0)
                    dbCmd.Parameters.AddWithValue("model", model);
                else
                    dbCmd.Parameters.AddWithValue("model", DBNull.Value);

                dbCmd.CommandText = "INSERT OR REPLACE INTO forecasts (stockId, ticker, risingPulse, upTrend, downTrend, validTrendCount, authenticity, currentPrice, target, potential, model) values (?,?,?,?,?,?,?,?,?,?,?)";
                dbCmd.ExecuteNonQuery();
                dbCmd.Parameters.Clear();

                WindowLog("Creating forecasts - " + progress + "/" + stocksTable.Rows.Count, true);
            }
            if (errors != 0)
                WindowLog("Forecasts created with " + errors + " error(s)");
            DisposeDbConnection(dbCmd);
            CreateForecastsExcelFile();
        }

        public static void CreateForecastsExcelFile()
        {
            SQLiteCommand dbCmd;
            DataTable forecastsTable = new DataTable();
            Microsoft.Office.Interop.Excel.Application ExcelApplication = new Microsoft.Office.Interop.Excel.Application();

            try
            {
                Directory.CreateDirectory(iniData["General"]["ForecastsPath"].ToString());
            }
            catch (Exception ex)
            {
                FileLog(GetExceptionMessage(ex));
                WindowLog("Unable to create directory. Maybe path is incorrect");
                return;
            }

            WindowLog("Creating excel file 0%");

            ExcelApplication.Application.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel.Worksheet workSheet = ExcelApplication.ActiveWorkbook.ActiveSheet;

            Microsoft.Office.Interop.Excel.Range range = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[2, 1]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 1] = "ID";

            range = workSheet.Range[workSheet.Cells[1, 2], workSheet.Cells[2, 2]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 2] = "Тикер";

            range = workSheet.Range[workSheet.Cells[1, 3], workSheet.Cells[2, 3]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 3] = "Наименование";

            range = workSheet.Range[workSheet.Cells[1, 4], workSheet.Cells[2, 4]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 4] = "Направление";

            range = workSheet.Range[workSheet.Cells[1, 5], workSheet.Cells[1, 6]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 5] = "Кол-во признаков тренда";
            workSheet.Cells[2, 5] = "Вверх";
            workSheet.Cells[2, 6] = "Вниз";

            range = workSheet.Range[workSheet.Cells[1, 7], workSheet.Cells[2, 7]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 7] = "Наличие тренда";

            range = workSheet.Range[workSheet.Cells[1, 8], workSheet.Cells[2, 8]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 8] = "Достоверность";

            range = workSheet.Range[workSheet.Cells[1, 9], workSheet.Cells[2, 9]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 9] = "Тек. цена";

            range = workSheet.Range[workSheet.Cells[1, 10], workSheet.Cells[2, 10]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 10] = "Цель";

            range = workSheet.Range[workSheet.Cells[1, 11], workSheet.Cells[2, 11]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 11] = "Потенциал (%)";

            range = workSheet.Range[workSheet.Cells[1, 12], workSheet.Cells[2, 12]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 12] = "Модель";

            range = workSheet.Range[workSheet.Cells[1, 13], workSheet.Cells[2, 13]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 13] = "Сектор";

            range = workSheet.Range[workSheet.Cells[1, 14], workSheet.Cells[2, 14]];
            range.Merge(Type.Missing);
            workSheet.Cells[1, 14] = "Индустрия";

            range = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[2, 14]];
            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            range.Font.Bold = true;

            range = workSheet.Range[workSheet.Cells[2, 1], workSheet.Cells[2, 14]];
            range.AutoFilter(1, Type.Missing, Microsoft.Office.Interop.Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);

            workSheet.Application.ActiveSheet.Rows[3].Select();
            workSheet.Application.ActiveWindow.FreezePanes = true;
            workSheet.Cells[1, 1].Select();

            workSheet.Columns[4].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            workSheet.Columns[4].Font.Bold = true;
            workSheet.Columns[5].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            workSheet.Columns[6].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            workSheet.Columns[7].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            dbCmd = GetDbConnection();
            dbCmd.CommandText = "SELECT * FROM forecasts LEFT JOIN stocks ON stocks.id = forecasts.stockId";
            forecastsTable.Load(dbCmd.ExecuteReader());
            DisposeDbConnection(dbCmd);

            int percentageCount = forecastsTable.Rows.Count;
            percentageCount = percentageCount + (percentageCount / 100);
            int index = 2;
            int percentage = 0;
            foreach (DataRow forecast in forecastsTable.Rows)
            {
                index++;
                workSheet.Cells[index, 1] = forecast["stockId"];
                workSheet.Hyperlinks.Add(ExcelApplication.Cells[index, 2], forecast["link"].ToString(), "", "", forecast["ticker"].ToString());
                workSheet.Cells[index, 3] = forecast["name"];
                workSheet.Cells[index, 4] = Convert.ToBoolean(forecast["risingPulse"]) ? "UP" : "DOWN";
                workSheet.Cells[index, 5] = forecast["upTrend"];
                workSheet.Cells[index, 6] = forecast["downTrend"];
                workSheet.Cells[index, 7] = Convert.ToBoolean(forecast["validTrendCount"]) ? "Да" : "Нет";
                workSheet.Cells[index, 8] = forecast["authenticity"];
                workSheet.Cells[index, 9] = forecast["currentPrice"];
                workSheet.Cells[index, 10] = forecast["target"];
                workSheet.Cells[index, 11] = forecast["potential"];
                workSheet.Cells[index, 12] = forecast["model"];
                workSheet.Cells[index, 13] = forecast["sector"];
                workSheet.Cells[index, 14] = forecast["industry"];

                if (!Convert.IsDBNull(forecast["potential"]))
                    if (Convert.ToDouble(forecast["potential"]) < 0)
                        ExcelApplication.Cells[index, 11].Font.Color = Color.Red;
                    else
                        ExcelApplication.Cells[index, 11].Font.Color = Color.Green;

                if (Convert.ToBoolean(forecast["risingPulse"]))
                    ExcelApplication.Cells[index, 4].Font.Color = Color.Green;
                else
                    ExcelApplication.Cells[index, 4].Font.Color = Color.Red;

                WindowLog("Creating excel file " + Math.Round((double)percentage++ / percentageCount * 100, 0) + "%", true);
            }

            workSheet.Columns.AutoFit();
            workSheet.Columns[4].ColumnWidth = 13;
            workSheet.Columns[5].ColumnWidth = 13;

            string dateTime = DateTime.Now.ToString();
            dateTime = dateTime.Replace(".", "-");
            dateTime = dateTime.Replace(":", "-");

            string filePath = iniData["General"]["ForecastsPath"].ToString() + "Forecast " + dateTime + ".xlsx";

            if (File.Exists(filePath))
                File.Delete(filePath);

            ExcelApplication.Application.ActiveWorkbook.SaveAs(filePath);
            ExcelApplication.Application.ActiveWorkbook.Close();
            ExcelApplication.Quit();

            WindowLog("Creating excel file 100%", true);
        }

        private static bool ComparePreviousData(int stockId, String tableName, long timeFrameStep, string timeframeValue)
        {
            HttpWebRequest request;
            SQLiteCommand dbCmd;
            JObject responseJson = null;
            long to = GetLastTimeStamp(tableName) - timeFrameStep;
            long from = to - (timeFrameStep * 2);
            long now = GetTimeStampFromDateTime(DateTime.Now);

            if (from < 0)
                from = 0;

            request = WebRequest.CreateHttp("https://tvc4.forexpros.com/079b34920c3c048f3adb1301e4ae07cc/" + now + "/7/7/18/history" + "?symbol=" + stockId + "&resolution=" + timeframeValue + "&from=" + from + "&to=" + to);
            request.Method = "GET";
            request.Accept = "*/*";
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36";

            try
            {
                string responseString = new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd();
                responseJson = JsonConvert.DeserializeObject<JObject>(responseString);
            }
            catch (Exception ex)
            {
                FileLog(GetExceptionMessage(ex));

                dbCmd = GetDbConnection(true);
                dbCmd.Parameters.AddWithValue("id", stockId);
                dbCmd.CommandText = "UPDATE stocks SET errors = 1 WHERE id = :id";
                dbCmd.ExecuteNonQuery();
                DisposeDbConnection(dbCmd);

                Thread.CurrentThread.Abort();
            }

            if ((string)responseJson["s"] != "ok")
                return true;

            dbCmd = GetDbConnection();
            DataTable previousDataTable = new DataTable();
            dbCmd.CommandText = "SELECT * from " + tableName + " WHERE timeStamp BETWEEN " + from + " AND " + to;
            previousDataTable.Load(dbCmd.ExecuteReader());
            DisposeDbConnection(dbCmd);

            if (previousDataTable.Rows.Count != ((JContainer)responseJson["t"]).Count)
                return true;

            int index = 0;
            foreach (long timeStamp in responseJson["t"])
            {
                if ((double)previousDataTable.Rows[index]["c"] != (double)responseJson["c"][index])
                    return true;
                if ((double)previousDataTable.Rows[index]["o"] != (double)responseJson["o"][index])
                    return true;
                if ((double)previousDataTable.Rows[index]["h"] != (double)responseJson["h"][index])
                    return true;
                if ((double)previousDataTable.Rows[index]["l"] != (double)responseJson["l"][index])
                    return true;

                if (responseJson["v"][index].ToString() != "n/a")
                {
                    if (Convert.ToInt64(previousDataTable.Rows[index]["v"]) != (long)responseJson["v"][index])
                        return true;
                }

                index++;
            }

            return false;
        }

        public static void UpdateExchangesTable(DataTable exchangesTable)
        {
            SQLiteCommand dbCmd = GetDbConnection(true);
            dbCmd.CommandText = "DELETE FROM exchanges";
            dbCmd.ExecuteNonQuery();

            foreach (DataRow exchange in exchangesTable.Rows)
            {
                dbCmd.Parameters.AddWithValue("id", exchange["id"]);
                dbCmd.Parameters.AddWithValue("country", exchange["country"]);
                dbCmd.Parameters.AddWithValue("name", exchange["name"]);
                dbCmd.Parameters.AddWithValue("inactive", exchange["inactive"]);
                dbCmd.CommandText = "INSERT INTO exchanges (id, country, name, inactive) values (?,?,?,?)";
                dbCmd.ExecuteNonQuery();
                dbCmd.Parameters.Clear();
            }
            DisposeDbConnection(dbCmd);

            CleanupBase();
            UpdateFields();
        }
    }
}

﻿using System;
using System.Data;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace MarketDataParser
{
    public partial class MainWindow : Window
    {
        System.Windows.Forms.NotifyIcon trayIcon;
        WindowState prevWindowState;
        bool exitAtEnd;

        public MainWindow()
        {
            InitializeComponent();
            Program.InitializeProgram();
            Program.CreateDataBaseIfNotExist();

            exitAtEnd = false;

            trayIcon = new System.Windows.Forms.NotifyIcon();
            trayIcon.Icon = System.Drawing.Icon.ExtractAssociatedIcon("MDP.exe");
            trayIcon.DoubleClick += new EventHandler(trayIcon_DoubleClick);

            trayIcon.ContextMenu = new System.Windows.Forms.ContextMenu();
            trayIcon.ContextMenu.MenuItems.Add("Exit").Click += new EventHandler(trayIconContextMenu_Click);

            string[] arguments = Environment.GetCommandLineArgs();
            bool stocks = false;
            bool marketData = false;
            bool forecast = false;
            bool dividentsCalendar = false;
            bool sharingOutstanding = false;

            foreach (string argument in arguments)
            {
                if (argument.ToLower() == "/stocks")
                    stocks = true;
                if (argument.ToLower() == "/marketdata")
                    marketData = true;
                if (argument.ToLower() == "/forecast")
                    forecast = true;
                if (argument.ToLower() == "/dividentscalendar")
                    dividentsCalendar = true;
                if (argument.ToLower() == "/sharingoutstanding")
                    sharingOutstanding = true;
            }

            if (stocks == true || marketData == true || forecast == true)
            {
                HideToTray();
                StartAsync(stocks, marketData, dividentsCalendar, sharingOutstanding, forecast);
                exitAtEnd = true;
            }
        }

        //window
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshStocksDataGridAsync();
            RefreshExchangesDataGridAsync();
        }

        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            StartAsync((bool)stocksCheckBox.IsChecked, (bool)marketDataCheckBox.IsChecked, (bool)dividentsCalendarCheckBox.IsChecked, (bool)sharingOutstandingCheckBox.IsChecked, (bool)createForecastCheckBox.IsChecked);
        }

        private void trayIcon_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            trayIcon.Visible = false;
            WindowState = prevWindowState;
            exitAtEnd = false;
        }

        private void trayIconContextMenu_Click(object sender, EventArgs e)
        {
            CloseProgram();
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
            {
                HideToTray();
            }
            else
                prevWindowState = WindowState;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            trayIcon.Dispose();
        }

        //exchangesgrid
        private void exchangesDataGridMenuItem_click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            DataView dataView = exchangesDataGrid.ItemsSource as DataView;
            int selectedRowIndex = exchangesDataGrid.Items.IndexOf(exchangesDataGrid.CurrentItem);
            int selectedColumnIndex = exchangesDataGrid.CurrentCell.Column.DisplayIndex;
            switch (menuItem.Header)
            {
                case "Enable\\Disable":
                    long inactive = (long)((DataRowView)exchangesDataGrid.CurrentItem).Row.ItemArray[3];
                    ((DataRowView)exchangesDataGrid.CurrentItem).Row.SetField(3, inactive == 0 ? 1 : 0);
                    WriteExchangesDataGridAsync();
                    break;
                case "Delete":
                    dataView.Table.Rows.RemoveAt(exchangesDataGrid.Items.IndexOf(exchangesDataGrid.CurrentItem));
                    WriteExchangesDataGridAsync();
                    break;
                case "Copy":
                    Clipboard.SetText(((DataRowView)exchangesDataGrid.CurrentItem).Row.ItemArray[selectedColumnIndex].ToString());
                    break;
            }
        }

        private void exchangesDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            WriteExchangesDataGridAsync();
        }

        private void exchangesDataGrid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            bool isEnabled = exchangesDataGrid.CurrentItem != null && exchangesDataGrid.CurrentItem.GetType() == typeof(DataRowView) && !((DataRowView)exchangesDataGrid.CurrentItem).IsNew;
            ((MenuItem)exchangesDataGrid.ContextMenu.Items[0]).IsEnabled = isEnabled && startButton.IsEnabled;
            ((MenuItem)exchangesDataGrid.ContextMenu.Items[1]).IsEnabled = isEnabled && startButton.IsEnabled;
            ((MenuItem)exchangesDataGrid.ContextMenu.Items[2]).IsEnabled = isEnabled;
        }
        
        private void exchangesDataGrid_InitializingNewItem(object sender, InitializingNewItemEventArgs e)
        {
            if (((DataRowView)e.NewItem).Row.IsNull(3))
                ((DataRowView)e.NewItem).Row.SetField(3, (long)0);
        }

        //stocks
        private void stocksDataGridMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            DataView dataView = stocksDataGrid.ItemsSource as DataView;

            switch (menuItem.Header)
            {
                case "Open in browser":
                    System.Diagnostics.Process.Start(((DataRowView)stocksDataGrid.CurrentItem).Row.ItemArray[3].ToString());
                    break;
                case "Copy":
                    int selectedColumnIndex = stocksDataGrid.CurrentCell.Column.DisplayIndex;
                    Clipboard.SetText(((DataRowView)stocksDataGrid.CurrentItem).Row.ItemArray[selectedColumnIndex].ToString());
                    break;
                case "Refresh":
                    RefreshStocksDataGridAsync();
                    break;
            }
        }

        private void stocksDataGrid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            bool isEnabled = stocksDataGrid.CurrentItem != null && stocksDataGrid.CurrentItem.GetType() == typeof(DataRowView);
            ((MenuItem)stocksDataGrid.ContextMenu.Items[1]).IsEnabled = isEnabled;
            ((MenuItem)stocksDataGrid.ContextMenu.Items[2]).IsEnabled = isEnabled;
        }

        //other
        private void EnableConrols(bool enable)
        {
            stocksCheckBox.IsEnabled = enable;
            createForecastCheckBox.IsEnabled = enable;
            marketDataCheckBox.IsEnabled = enable;
            sharingOutstandingCheckBox.IsEnabled = enable;
            dividentsCalendarCheckBox.IsEnabled = enable;
            startButton.IsEnabled = enable;

            exchangesDataGrid.IsReadOnly = !enable;
        }

        private void HideToTray()
        {
            WindowState = WindowState.Minimized;
            this.Hide();
            trayIcon.Visible = true;
        }

        private void CloseProgram()
        {
            this.Close();
            Application.Current.Shutdown();
        }

        private async void StartAsync(bool stocks, bool marketData, bool dividentsCalendar, bool sharingOutstanding, bool forecast)
        {
            EnableConrols(false);
            await Task.Run(() => Program.UpdateData(stocks, marketData, dividentsCalendar, sharingOutstanding, forecast));
            RefreshStocksDataGridAsync();

            if (exitAtEnd)
                CloseProgram();

            EnableConrols(true);
        }

        public async void RefreshStocksDataGridAsync()
        {
            DataTable dataTable = await Task.Run(() => Program.GetDataTable("SELECT * FROM stocks"));
            stocksDataGrid.ItemsSource = dataTable.DefaultView;
        }
        
        public async void RefreshExchangesDataGridAsync()
        {
            DataTable dataTable = await Task.Run(() => Program.GetDataTable("SELECT * FROM exchanges"));
            exchangesDataGrid.ItemsSource = dataTable.DefaultView;
        }
        
        private async void WriteExchangesDataGridAsync()
        {
            DataView dataView = exchangesDataGrid.ItemsSource as DataView;
            await Task.Run(() => Program.UpdateExchangesTable(dataView.Table));
            RefreshStocksDataGridAsync();
        }
    }
}
